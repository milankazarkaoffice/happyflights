//
//  ViewController.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/4/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit
import DatePickerDialog

class ViewController: UIViewController, UITextViewDelegate, StationsSelectorProtocol {

    @IBOutlet weak var activity: UIActivityIndicatorView?
    
    @IBOutlet weak var contentHolder : UIView?
    
    var scrollView = UIScrollView()
    
    var stationsSelector = StationsSelector()
    
    var indexOfSelectedStation = Int(-1)
    
    var fromLbl = UILabel()
    var fromText = UITextView()
    
    var toLbl = UILabel()
    var toText = UITextView()
    
    var activeText : UITextView?
    var selectedFromStation : Station?
    var selectedToStation : Station?
    
    var adtLbl = UILabel()
    var teenLbl = UILabel()
    var chdLbl = UILabel()
    
    var adtSwitch = upDownSwitch()
    var teenSwitch = upDownSwitch()
    var chdSwitch = upDownSwitch()
    
    var selectedDay = Int(-1)
    var selectedMonth = Int(-1)
    var selectedYear = Int(-1)
    
    var dateLbl = UILabel()
    var dateBtn = UIButton()
    
    var searchBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stationsSelector.delegate = self
        contentHolder?.addSubview(scrollView)
        setContent()
        activity?.startAnimating()
        NotificationCenter.default.addObserver(
            self, selector: #selector(self.stationsLoaded),
            name: NSNotification.Name(rawValue: Common.stationsLoadedNotif),
            object: nil)
        if let _ = Stations.sharedInstance as Stations? { // inits stations automatically
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func stationsLoaded( ) {
        print("ViewController data changed")
        activity?.stopAnimating()
        activity?.isHidden = true
        self.contentHolder?.isHidden = false
    }
    
    func setContent() {
        
        scrollView.frame = (self.contentHolder?.bounds)!
        scrollView.backgroundColor = UIColor.gray
        
        let border : CGFloat = 20.0
        var yset : CGFloat = border
        fromLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        fromLbl.textColor = UIColor.white
        fromLbl.textAlignment = NSTextAlignment.right
        fromLbl.text = "From:"
        scrollView.addSubview(fromLbl)
        fromText.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        fromText.delegate = self
        fromText.backgroundColor = UIColor.darkGray
        fromText.layer.cornerRadius = 5.0
        fromText.textColor = UIColor.white
        fromText.font = fromLbl.font
        fromText.returnKeyType = UIReturnKeyType.done
        scrollView.addSubview(fromText)
        
        yset += fromLbl.bounds.size.height+10.0
        
        toLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        toLbl.textColor = UIColor.white
        toLbl.textAlignment = NSTextAlignment.right
        toLbl.text = "To:"
        scrollView.addSubview(toLbl)
        toText.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        toText.delegate = self
        toText.backgroundColor = UIColor.darkGray
        toText.layer.cornerRadius = 5.0
        toText.textColor = UIColor.white
        toText.font = toLbl.font
        toText.returnKeyType = UIReturnKeyType.done
        scrollView.addSubview(toText)
        
        yset += toLbl.bounds.size.height+10.0
        
        adtLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        adtLbl.textColor = UIColor.white
        adtLbl.textAlignment = NSTextAlignment.right
        adtLbl.text = "Adults:"
        scrollView.addSubview(adtLbl)
        adtSwitch.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        adtSwitch.countLbl.text = "1"
        adtSwitch.number = 1
        scrollView.addSubview(adtSwitch)
        
        yset += adtLbl.bounds.size.height+10.0
        
        teenLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        teenLbl.textColor = UIColor.white
        teenLbl.textAlignment = NSTextAlignment.right
        teenLbl.text = "Teens:"
        scrollView.addSubview(teenLbl)
        teenSwitch.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        scrollView.addSubview(teenSwitch)
        
        yset += teenLbl.bounds.size.height+10.0
        
        chdLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        chdLbl.textColor = UIColor.white
        chdLbl.textAlignment = NSTextAlignment.right
        chdLbl.text = "Children:"
        scrollView.addSubview(chdLbl)
        chdSwitch.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        scrollView.addSubview(chdSwitch)
        
        yset += chdLbl.bounds.size.height+10.0
        
        dateLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        dateLbl.textColor = UIColor.white
        dateLbl.textAlignment = NSTextAlignment.right
        dateLbl.text = "Date:"
        scrollView.addSubview(dateLbl)
        dateBtn.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        dateBtn.setTitle("Select day", for: UIControlState.normal)
        dateBtn.backgroundColor = UIColor.black
        dateBtn.layer.cornerRadius = 5.0
        dateBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        
        dateBtn.addTarget(self,
                            action: #selector(onSelectDate),
                            for: .touchUpInside
        )
        
        scrollView.addSubview(dateBtn)
        
        yset += dateLbl.bounds.size.height+10.0
        
        searchBtn.frame = CGRect(x:border,y:yset,width:scrollView.frame.size.width-(border*2.0),height:55.0)
        searchBtn.layer.cornerRadius = 5.0
        searchBtn.backgroundColor = UIColor.white
        searchBtn.setTitleColor(UIColor.blue, for: UIControlState.normal)
        searchBtn.setTitle("Search", for: UIControlState.normal)
        scrollView.addSubview(searchBtn)
        
        searchBtn.addTarget(self,
                        action: #selector(onSearch),
                        for: .touchUpInside
        )
        
        yset += searchBtn.bounds.size.height+10.0
        
        scrollView.contentSize = CGSize(width: scrollView.bounds.size.width, height: yset)
        
        stationsSelector.isHidden = true
        scrollView.addSubview(stationsSelector)
    }
    
    override func viewDidLayoutSubviews() {
        print("ViewController viewDidLayoutSubviews")
        
        scrollView.contentOffset = CGPoint(x: 0.0, y: 0.0)
        scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        
        scrollView.frame = (self.contentHolder?.bounds)!
        
        let border : CGFloat = 20.0
        var yset : CGFloat = border
        fromLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        fromText.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        
        yset += fromLbl.bounds.size.height+10.0
        
        toLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        toText.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        
        yset += toLbl.bounds.size.height+10.0
        
        adtLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        adtSwitch.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        
        yset += adtLbl.bounds.size.height+10.0
        
        teenLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        teenSwitch.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        
        yset += teenLbl.bounds.size.height+10.0
        
        chdLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        chdSwitch.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        
        yset += chdLbl.bounds.size.height+10.0
        
        dateLbl.frame = CGRect(x:border,y:yset,width:((scrollView.frame.size.width-(border*2.0))/3.0)-border,height:35.0)
        dateBtn.frame = CGRect(x:((scrollView.frame.size.width-(border*2.0))/3.0)+border,y:yset,width:(scrollView.frame.size.width-(border*2.0))*(2.0/3.0),height:35.0)
        
        yset += dateLbl.bounds.size.height+10.0
        
        searchBtn.frame = CGRect(x:border,y:yset,width:scrollView.frame.size.width-(border*2.0),height:55.0)
        
        yset += searchBtn.bounds.size.height+10.0
        
        scrollView.contentSize = CGSize(width: scrollView.bounds.size.width, height: yset)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        activeText = textView
        
        textView.text = ""
        if activeText == fromText {
            selectedFromStation = nil
        } else {
            selectedToStation = nil
        }
        
        let border : CGFloat = 20.0
        stationsSelector.frame = CGRect(x:border,y:textView.frame.origin.y+textView.frame.size.height+5.0,width:scrollView.frame.size.width-(border*2.0),height:120.0)
        scrollView.contentOffset = CGPoint(x:0.0,y:textView.frame.origin.y-5.0)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if stationsSelector.isHidden == false {
            stationsSelector.isHidden = true
        }
        scrollView.contentOffset = CGPoint(x:0.0,y:0.0)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("text(\(textView.text))")
        
        if textView.text.characters.count > 0 {
            if stationsSelector.isHidden == true {
                stationsSelector.isHidden = false
            }
            
            activeText = textView
            let foundStations = Stations.sharedInstance.stationsForSearchTerm(searchTerm: textView.text)
            stationsSelector.loadStations(stations: foundStations)
        
        } else if textView.text.characters.count == 0 {
            if stationsSelector.isHidden == false {
                stationsSelector.isHidden = true
            }
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func stationSelected(station: Station) {
        print("selected station")
        
        if activeText == fromText {
            selectedFromStation = station
        } else {
            selectedToStation = station
        }
        
        if let code = station.myCode {
            activeText!.text = code
        }
        
        activeText?.resignFirstResponder()
        scrollView.contentOffset = CGPoint(x:0.0,y:0.0)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            scrollView.contentOffset = CGPoint(x:0.0,y:0.0)
            return false
        }
        return true
    }
    
    func onSelectDate() {
        print("onSelectDate")
        
        DatePickerDialog().show(title: "DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            print("date selected (\(date))")
            let calendar = Calendar.current
            self.selectedYear = calendar.component(.year, from: date!)
            self.selectedMonth = calendar.component(.month, from: date!)
            self.selectedDay = calendar.component(.day, from: date!)
            
            self.dateBtn.setTitle("\(self.selectedMonth) - \(self.selectedDay) - \(self.selectedYear)", for: UIControlState.normal)
        }
    }
    
    func onSearch() {
        print("onSearch")
        
        // lazy check
        if selectedToStation != nil && selectedFromStation != nil && selectedDay != -1 {
            let searchAttributes = SearchAttributes()
            
            searchAttributes.fromStation = selectedFromStation
            searchAttributes.toStation = selectedToStation
            
            searchAttributes.adtCount = adtSwitch.number
            searchAttributes.teenCount = teenSwitch.number
            searchAttributes.chdCount = chdSwitch.number
            
            searchAttributes.flightDay = selectedDay
            searchAttributes.flightMonth = selectedMonth
            searchAttributes.flightYear = selectedYear
                        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier :"SearchResultsViewController") as! SearchResultsViewController
            viewController.searchAttributes = searchAttributes
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else {
            print("didn't fill out all the fields")
        }
    }
}

