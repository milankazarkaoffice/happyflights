//
//  upDownSwitch.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/6/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class upDownSwitch: UIView {

    let downBtn = UIButton()
    let countLbl = UILabel()
    let upBtn = UIButton()
    var number = Int(0)
    
    init() {
        super.init(frame: CGRect(x:0.0,y:0.0,width:0.0,height:0.0))
        
        self.clipsToBounds = true
        self.layer.cornerRadius = 5.0
        
        self.addSubview(downBtn)
        self.addSubview(countLbl)
        self.addSubview(upBtn)
        
        downBtn.backgroundColor = UIColor.black
        downBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        downBtn.setTitle("-", for: UIControlState.normal)
        countLbl.backgroundColor = UIColor.darkGray
        countLbl.text = "0"
        countLbl.textColor = UIColor.white
        countLbl.textAlignment = NSTextAlignment.center
        upBtn.backgroundColor = UIColor.black
        upBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        upBtn.setTitle("+", for: UIControlState.normal)
        
        downBtn.addTarget(self,
                          action: #selector(onDown),
                          for: .touchUpInside
        )
        upBtn.addTarget(self,
                          action: #selector(onUp),
                          for: .touchUpInside
        )
    }
    
    func setLabelToNumber()
    {
        countLbl.text = "\(number)"
    }
    
    func onDown()
    {
        if number > 0 {
            number -= 1
        }
        setLabelToNumber()
    }
    
    func onUp()
    {
        if number < Common.maxPassengers {
            number+=1
        }
        setLabelToNumber()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        downBtn.frame = CGRect(x:0.0,y:0.0,width:self.frame.size.width/3.0,height:self.frame.size.height)
        countLbl.frame = CGRect(x:self.frame.size.width/3.0,y:0.0,width:self.frame.size.width/3.0,height:self.frame.size.height)
        upBtn.frame = CGRect(x:self.frame.size.width*(2.0/3.0),y:0.0,width:self.frame.size.width/3.0,height:self.frame.size.height)
    }
}
