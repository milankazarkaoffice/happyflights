//
//  StationsSelector.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/6/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

protocol StationsSelectorProtocol {
    func stationSelected(station:Station)
}

class StationsSelector: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var delegate:StationsSelectorProtocol?
    let tableView = UITableView()
    var myStations : Array<Station>?
    
    init() {
        super.init(frame: CGRect(x:0.0,y:0.0,width:0.0,height:0.0))
        self.layer.cornerRadius = 3.0
        self.clipsToBounds = true
        self.backgroundColor = UIColor.white
        self.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tableView.frame = self.bounds
    }
    
    func loadStations(stations:Array<Station>) {
        myStations = stations
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if myStations != nil {
            return (myStations?.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            delegate!.stationSelected(station: myStations![indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style:UITableViewCellStyle.default, reuseIdentifier: "generic")
        if let station = myStations?[indexPath.row] {
            if let code = station.myCode, let name = station.myName {
                cell.textLabel?.text = code + " " + name
            }
    
        }
        return cell;
    }
}
