//
//  Stations.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/4/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Stations: NSObject {
    static let sharedInstance = Stations()
    
    var stations: Array<Station> = Array()
    
    override init( ) {
        super.init()
        reload()
    }
    
    /** return a list of Station objects based on a general search term we entered
    */
    func stationsForSearchTerm(searchTerm: String) -> Array<Station> {
        var selectedStations = Array<Station>()
        for i in (0..<stations.count) {
            let station = stations[i] as Station
            
            // todo - myCode shouldn't be forced but checked
            
            if searchTerm.characters.count <= (station.myCode?.characters.count)! {
                
                if let index = station.myCode?.index((station.myCode?.startIndex)!, offsetBy: searchTerm.characters.count) {
                    if let substring = station.myCode?.substring(to: index) {
                        print("substring(\(substring))")
                        
                        if substring.lowercased() == searchTerm.lowercased() {
                            selectedStations.append(station)
                        }
                        
                    }
                }
            }
        }
        return selectedStations
    }
    
    func reload( ) {
        guard let url = URL(string: Common.stationsUrlString) else {
            print("couldn't create URL for stations call")
            return
        }
        
        let theRequest = URLRequest(url: url)
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: theRequest, completionHandler: { (data, response, error) in
            print("response(\(response)) error(\(error))")
            if let data = data as Data? {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                    
                    DispatchQueue.main.async {
                        let lockQueue = DispatchQueue(label: "com.dataLock")
                        lockQueue.sync() {
                            
                            self.stations.removeAll()
                            
                            if let stationsObject = parsedData as? NSDictionary {
                                if let stationsArray = stationsObject.object(forKey: "stations") as? NSArray {
                                    for i in (0..<stationsArray.count) {
                                        if let stationDef = stationsArray.object(at: i) as? NSDictionary {
                                            let ostation = Station(withDictionary:stationDef)
                                            self.stations.append(ostation)
                                        }
                                    }
                                }
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Common.stationsLoadedNotif), object: nil)
                            }
                            
                        }
                    }
                }
                catch let error as NSError {
                    print("Details of JSON parsing error:\n \(error)")
                }
            }
        })
        task.resume()
    }
}
