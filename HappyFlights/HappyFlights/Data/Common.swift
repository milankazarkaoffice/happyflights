//
//  Common.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/4/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Common: NSObject {
    static let stationsUrlString : String = "https://tripstest.ryanair.com/static/stations.json"
    static let stationsLoadedNotif : String = "stationsLoadedNotif"
    static let searchResultsReadyNotif : String = "searchResultsReadyNotif"
    static let raBlueColor = UIColor.init(red: 8.0/255.0, green: 53.0/255.0, blue: 144.0/255, alpha: 1.0)
    static let maxPassengers = Int(6)
}
