//
//  SearchAttributes.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/6/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class SearchAttributes: NSObject {
    
    var fromStation : Station?
    var toStation : Station?
    
    var adtCount = Int(-1)
    var teenCount = Int(-1)
    var chdCount = Int(-1)
    
    var flightDay = Int(-1)
    var flightMonth = Int(-1)
    var flightYear = Int(-1)
}
