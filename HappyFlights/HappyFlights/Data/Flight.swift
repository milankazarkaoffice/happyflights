//
//  Flight.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/6/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Flight: NSObject {

    var rawAttributes : NSDictionary = NSDictionary()
    var shortDate : String? // just the date
    var flightNumber : String?
    var regularFareAmount = Double(0.0)
    
    init( withDictionary: NSDictionary, date: String ) {
        super.init()
        rawAttributes = withDictionary
        let index = date.index(date.startIndex, offsetBy: 10)
        shortDate = date.substring(to: index)
        
        let number : String? = getAttribute(key: "flightNumber", type: String.self)
        if number != nil {
            flightNumber = number
        }
        
        let regular : NSDictionary? = getAttribute(key: "regularFare", type: NSDictionary.self)
        if regular != nil {
            
            if let fares = regular!.object(forKey:"fares" ) as? NSArray {
            
                for i in (0..<fares.count) {
                    if let fareDef = fares.object(at: i) as? NSDictionary {
                        print("we got fare data")
                        if let amount = fareDef.object(forKey:"amount") as? Double {
                            regularFareAmount = amount
                        }
                    }
                }
            }
        }
        
        print("got the basics")
    }
    
    // lazy approach, hardcoded the dictionary - won't work for nested objects
    func getAttribute<T>(key: String, type: T.Type) -> T? {
        if let strValue = rawAttributes.object(forKey:key ) as? T {
            return strValue
        }
        return nil
    }
}
