//
//  Station.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/4/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class Station: NSObject {
    
    var rawAttributes : NSDictionary = NSDictionary()
    var myCode : String?
    var myName : String?
    
    init( withDictionary: NSDictionary ) {
        super.init()
        rawAttributes = withDictionary
        let code : String? = getAttribute(key: "code", type: String.self)
        if code != nil {
            myCode = code
        }
        let name : String? = getAttribute(key: "name", type: String.self)
        if name != nil {
            myName = name
        }
    }
    
    // lazy approach
    func getAttribute<T>(key: String, type: T.Type) -> T? {
        if let strValue = rawAttributes.object(forKey:key ) as? T {
            return strValue
        }
        return nil
    }
}
