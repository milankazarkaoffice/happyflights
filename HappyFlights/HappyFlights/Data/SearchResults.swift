//
//  SearchResults.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/6/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class SearchResults: NSObject {
    static let sharedInstance = SearchResults()
    
    var flights: Array<Flight> = Array()
    var myRun = Int(0)
    
    func search(attr: SearchAttributes) {
        
        myRun+=1
        let run = myRun
        
        print("attempting to search")
        
        let fromCode = attr.fromStation?.myCode
        let toCode = attr.toStation?.myCode
        
        // url could be constructed nicely
        let urlString = "https://sit-nativeapps.ryanair.com/v4/Availability?origin=\(fromCode!)&destination=\(toCode!)&dateout=\(attr.flightYear)-\(attr.flightMonth)-\(attr.flightDay)&datein=&flexdaysbeforeout=3&flexdaysout=3&flexdaysbeforein=3&flexdaysin=3&adt=\(attr.adtCount)&teen=\(attr.teenCount)&chd=\(attr.chdCount)&roundtrip=false"
        
        guard let url = URL(string: urlString) else {
            print("couldn't create URL for stations call")
            return
        }
        
        let theRequest = URLRequest(url: url)
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: theRequest, completionHandler: { (data, response, error) in
            print("response(\(response)) error(\(error))")
            if let data = data as Data? {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments)
                    
                    DispatchQueue.main.async {
                        let lockQueue = DispatchQueue(label: "com.dataLock")
                        lockQueue.sync() {
                            
                            if run != self.myRun {
                                return
                            }
                            
                            self.flights.removeAll()
                            
                            var currentDateString : String?
                            
                            if let superDict = parsedData as? NSDictionary {
                                if let array = superDict.object(forKey: "trips") as? NSArray {
                                    
                                    print("returned trips(\(array.count)) objects")
                                    
                                    // ugly nested way for now. No time to do it more nicely at this point
                                    
                                    for i in (0..<array.count) {
                                        if let trip = array.object(at: i) as? NSDictionary {
                                            
                                            if let datesArray = trip.object(forKey: "dates") as? NSArray {
                                                
                                                for ii in (0..<datesArray.count) {
                                                    if let date = datesArray.object(at: ii) as? NSDictionary {
                                                        
                                                        if let dateOutAttr = date.object(forKey: "dateOut") as? String {
                                                            currentDateString = dateOutAttr
                                                        }

                                                        
                                                        if let flightsArray = date.object(forKey: "flights") as? NSArray {
                                                            
                                                            for fi in (0..<flightsArray.count) {
                                                                if let flight = flightsArray.object(at: fi) as? NSDictionary {
                                                                
                                                                    let oflight = Flight(withDictionary:flight, date: currentDateString!)
                                                                    self.flights.append(oflight)
                                                                    
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Common.searchResultsReadyNotif), object: nil)
                        }
                    }
                } catch let error as NSError {
                    print("Details of JSON parsing error:\n \(error)")
                }
            }
        })
        task.resume()
    }
}
