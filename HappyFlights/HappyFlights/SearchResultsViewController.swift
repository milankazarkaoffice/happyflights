//
//  SearchResultsViewController.swift
//  HappyFlights
//
//  Created by Milan Kazarka on 4/6/17.
//  Copyright © 2017 Milan Kazarka. All rights reserved.
//

import UIKit

class SearchResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView : UITableView?
    
    var searchAttributes : SearchAttributes?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Loading"
        if searchAttributes != nil {
            
            let lockQueue = DispatchQueue(label: "com.dataLock")
            lockQueue.sync() {
                
                SearchResults.sharedInstance.flights.removeAll()
            }
            tableView?.reloadData()
            
            NotificationCenter.default.addObserver(
                self, selector: #selector(self.newSearchResult),
                name: NSNotification.Name(rawValue: Common.searchResultsReadyNotif),
                object: nil)
            
            SearchResults.sharedInstance.search(attr: searchAttributes!)
        }
    }
    
    func newSearchResult() {
        self.title = "Results"
        tableView?.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        NSLog("rows")
        
        return SearchResults.sharedInstance.flights.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        NSLog("get cell")
        
        let cell = UITableViewCell(style:UITableViewCellStyle.subtitle, reuseIdentifier: "flightCell")
        
        let flight = SearchResults.sharedInstance.flights[indexPath.row]
        if flight.shortDate != nil {
            cell.textLabel?.text = flight.shortDate!
        }
        if flight.flightNumber != nil {
            cell.detailTextLabel?.text = flight.flightNumber! + " \(flight.regularFareAmount)"
        }
            
        return cell;
    }
}
